<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Incidencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('aula');
            $table->date('fecha');
            $table->string('codigo');
            $table->string('equipo');
            $table->string('descripcion');
            $table->string('estado');
            $table->string('mas_info')->default("En espera");
            $table->string('comentarios_admin')->default("En espera");
            $table->unsignedBigInteger('profesorID');
            $table->foreign('profesorID')
                    ->references('id')-> on('profesores')
                    ->onUpdate('cascade')
                    ->onDelete('cascade')
            ;
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('incidencias');
    }
}
