<style>

    h1{
      color:red;
    }
    
</style>

@extends('layouts.app2')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" align="center" >
                       
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <h1 align="center">{{$error}}</h1>
                        
                        <form action="home" method="get" id="form"><center><input class="form-btn" name="submit" type="submit" value="Home" /></center></form>
        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection