@extends('layouts.app')
<style>
        #menubar {
          list-style-type: none;
          margin: 0;
          padding: 0;
          overflow: hidden;
          border: 1px solid #e7e7e7;
          background-color: #f3f3f3;
        }
        
        #menubar {
          float: left;
        }
        
        #menubar a {
          display: block;
          color: #666;
          text-align: center;
          padding: 14px 16px;
          text-decoration: none;
        }
        
        #menubar a:hover:not(.active) {
          background-color: #ddd;
        }
        
        #menubar a.active {
          color: white;
          background-color: #4CAF50;
        }
</style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div id="menubar">  
                    <ul>
                    <li><a href="#">Incidencias</a></li>
                    <li><a href="#">Perfil</a></li>
                    <li><a href="#">Panel Admin</a></li>
                    <li><a href="#">Cerrar sesion</a></li>
                  </ul></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bienvenido, {{ session('status') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
