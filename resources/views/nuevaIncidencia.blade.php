@extends('layouts.app2')


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Nueva incidencia</div>
                <div class="col-md-2 col-md-offset-10">
                    <a class="btn "  href="{{route('inicio') }}"><strong>Volver</strong></a>
                </div>
                <div class="panel-body">

									<form class="form-horizontal" method="post" action="{{ route('guardarDatos') }}">
										{{ csrf_field() }}

									<div class="container">
									<div class="row">
                    <div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="fecha">Fecha:</label>
												</br>
											</div>
											<div class=" col-md-7">
                      	<input type="date" class="form-control" name="fecha" required>
												</br>
											</div>
										</div>

										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="nombre">Nombre:</label>
												</br>
											</div>
											<div class=" col-md-9">
                      	<input type="text" class="form-control" id="nombre" name="nombre" value="{{ Auth::user()->name }}" readonly="readonly">
												</br>
											</div>
										</div>
										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="nombre">profesorID:</label>
												</br>
											</div>
											<div class=" col-md-9">
                      	<input type="text" class="form-control" id="profesorID" name="profesorID" value="{{ Auth::user()->id }}" readonly="readonly">
												</br>
											</div>
										</div>

										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="aula">Aula:</label>
												</br>
											</div>
											<div class=" col-md-9">
                      	<input type="text" class="form-control" id="aula" placeholder="Aula de la incidencia" name="aula">
												</br>
											</div>
										</div>

										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="equipo">Equipo:</label>
												</br>
											</div>
											<div class=" col-md-9">
                      	<input type="text" class="form-control" id="equipo" placeholder="Equipo de la incidencia" name="equipo">
												</br>
											</div>
										</div>
</br>
										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="estado">Estado:</label>
												</br>
											</div>
											<div class=" col-md-9">
												<select name="estado" readonly="readonly">
													 <option value="Abierto" selected>Abierto</option>
													 <option value="En Proceso" disabled>En Proceso</option>
													 <option value="Resuelta" disabled>Resuelta</option>
													 <option value="Rechazada" disabled>Rechazada</option>
												</select>
</br>
											</div>
										</div>
</br>
										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="codigo">Codigo Averia:</label>
												</br>
											</div>
											<div class=" col-md-9">
												<select name="codigo">
                          <option value="1-No se enciende la CPU/ CPU ez da pizten">1-. No se enciende la CPU/ CPU ez da pizten</option>
                         <option value="2-No se enciende la pantalla/Pantaila ez da pizten">2-. No se enciende la pantalla/Pantaila ez da pizten</option>
                         <option value="3-No entra en mi sesión/ ezin sartu nere erabiltzailearekin">3-. No entra en mi sesión/ ezin sartu nere erabiltzailearekin</option>
                         <option value="4-No navega en Internet/ Internet ez dabil">4-. No navega en Internet/ Internet ez dabil</option>
                         <option value="5-No se oye el sonido/ Ez da aditzen">5-. No se oye el sonido/ Ez da aditzen</option>
                         <option value="6-No lee el DVD/CD">6-. No lee el DVD/CD</option>
                         <option value="7-Teclado roto/ Tekladu hondatuta">7-. Teclado roto/ Tekladu hondatuta</option>
                         <option value="8-No funciona el ratón/Xagua ez dabil">8-. No funciona el ratón/Xagua ez dabil</option>
                         <option value="9-Muy lento para entrar en la sesión/oso motel dijoa">9-. Muy lento para entrar en la sesión/oso motel dijoa</option>
                         <option value="10-(Otros) Especifica/Beste batzu">10-. (Otros) Especifica/Beste batzu</option>
												</select>
												</br>
											</div>
										</div>

										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="descripcion">Descripción:</label>
												</br>
											</div>
											<div class=" col-md-7">
                      	<textarea class="form-control" rows="5" id="descripcion" name="descripcion" placeholder="Descripción de la incidencia"></textarea>

											</div></br>
										</div></br>

										<div class="col-md-6 col-md-offset-1 text-center">
										<hr>
									
								    	<button type="submit" class="btn btn-primary">Enviar</button>

										</div>
									</div>
								</div>
								@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif

                </div>
                <hr>
            </div>
        </div>
    </div>
</div>


@endsection
