@extends('layouts.appAdmin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Inicio del Administrador</div>
                <div class="col-md-2 col-md-offset-10">
                  <a class="btn btn-info btn-md btn-block" href="{{ route('nuevaIncidencia') }}">Crear nueva incidencia</a>
                </div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="panel-heading">Lista de incidencias</div>    
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Nombre</th>
                          <th>Código</th>
                          <th>Aula</th>
                          <th>Equipo</th>
                          <th>Descripción</th>
                          <th>Estado</th>
                          <th>Mas Información</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($incidencias as $incidencia)

                        <tr>
                          <td>{{ $incidencia -> fecha }}</td>
                          <td>{{ $incidencia -> profesorID }}</td>
                          <td>{{ $incidencia -> codigo }}</td>
                          <td>{{ $incidencia -> aula }}</td>
                          <td>{{ $incidencia -> equipo }}</td>
                          <td>{{ $incidencia -> descripcion }}</td>
                          <td>{{ $incidencia -> estado }}</td>
                          <td>{{ $incidencia -> mas_info }}</td>
                          <td><a class="btn btn-lg btn-block" href="{{ route('editAdminIncidencia', ['id'=>$incidencia->id]) }}">Detalles </a></td>
                          <td>
                              <form action="{{ route('eliminarIncidencia') }}" method="post">
                                  @csrf
                                    <input type="hidden" class="form-control" name="id" value="{{ $incidencia->id }}"></input>
                                    <button class="btn btn-lg btn-block" type="submit">Borrar incidencia</button>
                              </form> 

                          </td>
                          
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>


                </div>
            </div>
        </div>
    </div>
</div>


@endsection
