@extends('layouts.app2')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" hidden>Incidencias</div>
              
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                <table align="center">
                    <tr>
                        <td>
                                <div>
                                        <a class="btn btn-primary" style="font-size: 30px;" href="{{ route('nuevaIncidencia') }}">Crear incidencia</a>
                    
                                      </div> <br>
                        </td><br>
                        <td>
                               
                        <td>
                                <div>
                                        <a class="btn btn-primary" style="font-size: 30px;" href="{{ route('verIncidencia') }}">Ver Lista de incidencias</a>
                   
                                     </div> <br>
                        </td>
                    </tr>
              
                </table>

                </div>
                <hr>
            </div>
        </div>
    </div>
</div>


@endsection
