@extends('layouts.app2')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Ver Incidencias</div>
                <div class="col-md-2 col-md-offset-10">
                    <a class="btn "  href="{{ route('inicio') }}"><strong>Volver</strong></a>
                </div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
										<table class="table table-striped">
									    <thead>
									      <tr>
									        <th>Fecha</th>
									        <th>Código</th>
									        <th>Aula</th>
													<th>Equipo</th>
													<th>Descripción</th>
													<th>Estado</th>
									      </tr>
									    </thead>
									    <tbody>
											@foreach ($incidencias as $incidencia)

									      <tr>
									        <td>{{ $incidencia->fecha }}</td>
									        <td>{{ $incidencia->codigo }}</td>
													<td>{{ $incidencia->aula }}</td>
													<td>{{ $incidencia->equipo }}</td>
													<td>{{ $incidencia->descripcion }}</td>
													<td>{{ $incidencia->estado }}</td>
                          <td>
							<form action="{{ route('detallesIncidencia') }}" method="post">
								@csrf
									<input type="hidden" class="form-control" name="id" value="{{ $incidencia->id }}"></input>
									<button class="btn btn-lg btn-block" type="submit">Detalles</button>
							</form>
							<form action="{{ route('eliminarIncidencia') }}" method="post">
									@csrf
										<input type="hidden" class="form-control" name="id" value="{{ $incidencia->id }}"></input>
										<button class="btn btn-lg btn-block" type="submit">Borrar incidencia</button>
							</form>   
						
						</td>
									      </tr>
												@endforeach
									    </tbody>
									  </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
