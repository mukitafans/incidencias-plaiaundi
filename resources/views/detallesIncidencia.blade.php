@extends('layouts.app2')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
			
              @foreach ($incidencia as $incidencia)
                <div class="panel-heading">Incidencia Nº {{ $incidencia->id }}</div>
                <div class="col-md-2 col-md-offset-10">
                    <a class="btn "  href="{{ route('verIncidencia') }}"><strong>Volver</strong></a>
                </div>
                <div class="panel-body">
									<form class="form-horizontal" method="post" action="{{ route('actualizarDatos') }}">
										{{ csrf_field() }}
										
									<div class="container">
									<div class="row">
							<input type="hidden" class="form-control" name="id" value="{{ $incidencia->id }}" required>
                    <div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="fecha">Fecha:</label>
												</br>
											</div>
											<div class=" col-md-7">
                      	<input type="date" class="form-control" name="fecha" value="{{ $incidencia->fecha }}" readonly="readonly" required>
												</br>
											</div>
										</div>
										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="nombre">Nombre Profesor:</label>
												</br>
											</div>
											<div class="col-md-6 col-md-offset-1">
												<div class=" col-md-9">
							  <input type="hidden" class="form-control" id="profesorID" name="profesorID" value="{{ Auth::user()->id }}" readonly="readonly" required>
													</br>
												</div>
											</div>
											<div class=" col-md-9">
                      	<input type="text" class="form-control" id="name" name="name" value="{{ Auth::user()->name }}"  readonly="readonly" required>
												</br>
											</div>
										</div>
										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="aula">Aula:</label>
												</br>
											</div>
											<div class=" col-md-9">
                      	<input type="text" class="form-control" id="aula" value="{{ $incidencia->aula }}" name="aula" required>
												</br>
											</div>
										</div>
										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="equipo">Equipo:</label>
												</br>
											</div>
											<div class=" col-md-9">
                      	<input type="text" class="form-control" id="equipo" value="{{ $incidencia->equipo }}" name="equipo" required>
												</br>
											</div>
										</div>
</br>
										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
												<label class="control-label" for="estado">Estado:</label>
												</br>
											</div>
											<div class=" col-md-9">
												<input type="text" class="form-control" id="estado" value="{{ $incidencia->estado }}" name="estado" readonly="readonly" required>
												<select  name="estado" >
													 <option value="Abierto" >Abierto</option>
													 <option value="En Proceso" disabled>En Proceso</option>
													 <option value="Resuelta" >Resuelta</option>
													 <option value="Rechazada" disabled>Rechazada</option>
												</select>
                      </br>
											</div>
										</div>
                  </br>
										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
                        </br>
												<label class="control-label" for="codigo">Codigo Averia:</label>
												</br>
											</div>
                      </br>
											<div class=" col-md-9">
											<input type="text" class="form-control" id="codigo" value="{{ $incidencia->codigo }}" name="codigo" readonly="readonly" required>
                      <select name="codigo" >
													 <option value="1-No se enciende la CPU/ CPU ez da pizten">1-. No se enciende la CPU/ CPU ez da pizten</option>
													 <option value="2-No se enciende la pantalla/Pantaila ez da pizten">2-. No se enciende la pantalla/Pantaila ez da pizten</option>
													 <option value="3-No entra en mi sesión/ ezin sartu nere erabiltzailearekin">3-. No entra en mi sesión/ ezin sartu nere erabiltzailearekin</option>
													 <option value="4-No navega en Internet/ Internet ez dabil">4-. No navega en Internet/ Internet ez dabil</option>
													 <option value="5-No se oye el sonido/ Ez da aditzen">5-. No se oye el sonido/ Ez da aditzen</option>
													 <option value="6-No lee el DVD/CD">6-. No lee el DVD/CD</option>
													 <option value="7-Teclado roto/ Tekladu hondatuta">7-. Teclado roto/ Tekladu hondatuta</option>
													 <option value="8-No funciona el ratón/Xagua ez dabil">8-. No funciona el ratón/Xagua ez dabil</option>
													 <option value="9-Muy lento para entrar en la sesión/oso motel dijoa">9-. Muy lento para entrar en la sesión/oso motel dijoa</option>
													 <option value="10-(Otros) Especifica/Beste batzu">10-. (Otros) Especifica/Beste batzu</option>
												</select>
												</br>
											</div>
										</div>
										<div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
                        </br>
												<label class="control-label" for="descripcion">Descripción:</label>
												</br>
											</div>
											<div class=" col-md-7">
                        </br>
                      	<textarea class="form-control" rows="5" id="descripcion" name="descripcion" placeholder="{{ $incidencia->descripcion }}"  required readonly="readonly">{{ $incidencia->descripcion }} </textarea>
											</div>
										</div>
                    <div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
                        </br>
												<label class="control-label" for="mas_info">Mas Información:</label>
												</br>
											</div>
											<div class=" col-md-7">
                        </br>
                      	<textarea class="form-control" rows="5" id="mas_info" name="mas_info" required placeholder="Información adicional">{{ $incidencia->mas_info }} </textarea>

											</div>
										</div>
                    @if ($incidencia->comentarios_admin!="")
                    <div class="col-md-6 col-md-offset-1">
											<div class=" col-md-3">
                        </br>
												<label class="control-label" for="comentarios_admin">Comentarios del Administrador:</label>
												</br>
											</div>
											<div class=" col-md-7">
                        </br>
                      	<textarea class="form-control" rows="5" id="comentarios_admin" name="comentarios_admin" placeholder="{{ $incidencia->comentarios_admin }}" readonly="readonly">{{ $incidencia->comentarios_admin }} </textarea>

											</div>
										</div>
					@endif
					
					@if ($errors->any())
						<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
						</div>
					@endif  

					<div class="col-md-6 col-md-offset-1 text-center">
					<hr>
                    <div class="col-md-3 col-md-offset-2">
                      <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                    <div class="col-md-3 col-md-offset-2">
                      <a class="btn btn-primary"  href="{{ URL::previous() }}">Cancelar</a>
                    </div>

										</div>
									</div>
								</div>
								@if ($errors->has('descripcion'))
                  <span class="help-block">
                      <strong>{{ $errors->first('descripcion') }}</strong>
                  </span>
              	@endif
							  </form>

                </div>
                <hr>
@endforeach
			
            </div>
        </div>
    </div>
</div>


@endsection
