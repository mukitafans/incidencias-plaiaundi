@extends('layouts.app')
<style>
        #menubar {
          list-style-type: none;
          margin: 0;
          padding: 0;
          overflow: hidden;
          border: 1px solid #e7e7e7;
          background-color: #f3f3f3;
        }
        
        #menubar {
          float: left;
        }
        
        #menubar a {
          display: block;
          color: #666;
          text-align: center;
          padding: 14px 16px;
          text-decoration: none;
        }
        
        #menubar a:hover:not(.active) {
          background-color: #ddd;
        }
        
        #menubar a.active {
          color: white;
          background-color: #4CAF50;
        }
</style>
@section('content')
<div class="container">
    <div >
        <div>
            <div class="card">
                <div id="menubar">  
                    <ul>
                    <table>
                      <tr>
                        <td>
                            <a href="inicio" target="principal">Incidencias</a>
                        </td>
                        <td>
                            <a href="#" target="principal" hidden>Perfil</a>
                          </td>
                          <td>
                              <a class="dropdown-item" href="{{ route('logout') }}"
                              onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                               {{ __('Cerrar sesion') }}
                           </a>

                           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                               @csrf
                               <iframe id="logoutframe" src="https://accounts.google.com/logout" style="display: none"></iframe>
                           </form>
                            </td>
                      </tr>
                    </table>
                    
                    
                    
                  </ul></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <iframe src="inicio" name="principal" target="principal" width="1080px" height="600px"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
