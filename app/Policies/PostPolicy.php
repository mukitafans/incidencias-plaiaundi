<?php

namespace App\Policies;

use App\profesores;
use App\incidencia;
use Illuminate\Auth\Access\HandlesAuthorization;


class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any profesores.
     *
     * @param  \App\profesores  $user
     * @return mixed
     */
    public function viewAny(profesores $user)
    {
        //
    }

    /**
     * Determine whether the user can view the profesores.
     *
     * @param  \App\profesores  $user
     * @param  \App\profesores  $profesores
     * @return mixed
     */
    public function view(profesores $user, incidencia $incidencias)
    {
        return $user->id === $incidencias->profesorID;
    }

    /**
     * Determine whether the user can create profesores.
     *
     * @param  \App\profesores  $user
     * @return mixed
     */
    public function create(profesores $user, incidencia $incidencias)
    {
        return $user->id === $incidencias->profesorID;
    }

    /**
     * Determine whether the user can update the profesores.
     *
     * @param  \App\profesores  $user
     * @param  \App\profesores  $profesores
     * @return mixed
     */
    public function update(profesores $user, incidencia $incidencias)
    {
        return $user->id === $incidencias->profesorID;
    }

    /**
     * Determine whether the user can delete the profesores.
     *
     * @param  \App\profesores  $user
     * @param  \App\profesores  $profesores
     * @return mixed
     */
    public function delete(profesores $user, incidencia $incidencias)
    {
        return $user->id === $incidencias->profesorID;
    }

    /**
     * Determine whether the user can restore the profesores.
     *
     * @param  \App\profesores  $user
     * @param  \App\profesores  $profesores
     * @return mixed
     */
    public function restore(profesores $user, profesores $profesores)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the profesores.
     *
     * @param  \App\profesores  $user
     * @param  \App\profesores  $profesores
     * @return mixed
     */
    public function forceDelete(profesores $user, profesores $profesores)
    {
        //
    }
}
