<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class incidencia extends Model
{
  protected $table = 'incidencias';
  protected $fillable = [
      'profesor_ID', 'aula', 'fecha','equipo', 'codigo', 'descripcion', 'estado', 'mas_info', 'comentarios_admin',
  ];
}