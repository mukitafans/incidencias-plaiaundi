<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class inicioController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function inicio(){
        return view('inicio');
    }

    public function inicioAdmin(){
			$incidencias = DB::select('Select * from incidencias');
      return view('panelAdmin', ['incidencias' => $incidencias]);
    }




}
