<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class inicioController extends Controller
{

    //Redirrecion a las diferentes vistas
    //EN DESUSO

    public function __construct(){
        $this->middleware('auth');
    }

    public function inicio(){
        return view('inicio');
    }

    public function inicioAdmin(){
        return view('inicioAdmin');
    }

	public function nIncidenciaForm(){
        return view('nuevaIncidencia');
    }


}
