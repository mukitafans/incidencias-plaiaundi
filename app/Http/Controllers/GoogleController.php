<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
//use Auth;

use Exception;

use App\profesores;

class GoogleController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()

    {
        return Socialite::driver('google')->redirect();
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function handleGoogleCallback()

    {

        try {
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }     
        //si el usuario no es de plaiaundi, se redirige a una vista de error   
        if(explode("@", $user->email)[1] !== 'plaiaundi.net'){
            return redirect()->to('/login');
        }        
        $existingUser = profesores::where('email', $user->email)->first();        
        if($existingUser){
            
            auth()->login($existingUser, true);
        } else {
            // si no existe el usuario, procede a crearlo
            $newUser                  = new profesores;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->google_id       = $user->id;
            $newUser->avatar          = $user->avatar;
            $newUser->avatar_original = $user->avatar_original;
            $newUser->save();            auth()->login($newUser, true);
        }
        return redirect()->to('/home');


        /*
        try {
            $user = Socialite::driver('google')->user();
            $finduser = User::where('google_id', $user->id)->first();

            if($finduser){

                Auth::login($finduser);

                return redirect('/home');

            }else{

                $newUser = User::create([

                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,

                   // 'password' => encrypt('123456dummy')

                ]);

                Auth::login($newUser);

                return redirect('/home');

            }

        } catch (Exception $e) {

            dd($e->getMessage());

        }*/

    }
}
