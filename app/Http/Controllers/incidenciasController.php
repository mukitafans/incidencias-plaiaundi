<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Mail;
use Auth;
use Illuminate\Mail\Message;
use App\incidencia as incidencia;
use App\profesores;
use App\incidencias;

use DB;
class incidenciasController extends Controller
{


	//Redirige a la respectiva vista de creacion de incidencia
    public function formNuevaInci(){
        return view('nuevaIncidencia');
    }


	//Seleccion de las incidencias y lo muestra en pantalla
	public function verInci(){
      	//$dato = Auth::user()->dato;
			
	  	//$incidencias = DB::select('Select * from incidencias where profesorID = "' .$dato. '"');
	$incidencias = incidencia::select('id','fecha','codigo','aula','equipo','descripcion','estado')-> where('profesorID',Auth::user()->id)->get();
		
			

      return view('verIncidencia', ['incidencias' => $incidencias]);
    }

	//Guarda los datos de el formulario de creacion de incidencias
    public function guardarDatos (Request $request){
		//$post = incidencia::find($request->id);
        //$this->authorize('create', $post);

		if(Auth::user()->id!=$request->profesorID){
            return view("VistaError",['error'=>"No puedes crear la incidencia con el nombre de otro usuario"]);
        }
			
		$validacion = Validator::make($request->all(), [
			'equipo' => [
				'required','min:3','max:10','regex:/^HZ[0-9]*$/',
			],
			'aula' =>[
				'required','min:3','max:5','regex:/^[0-9]*$/',
			],
			'codigo' =>[
				'required'
			],
			'descripcion' =>[
				'max:255',
			],
			'profesorID' =>[
				'required',
			],
			'fecha' => [
				//'required','min:10','max:10','regex:/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/',
			],
		],
		[
			
			'equipo.regex' => 'El equipo debe tener HZ mayuscula y un maximo de 9 numeros',
			'fecha.regex' => 'El formato de fecha no es valido',
			'aula.regex' => 'Solo puedes poner aulas existentes',
			'codigo.in' =>'El codigo no es valido',
			'descripcion.max' => 'La descripcion admite :max caracteres como maximo',
			'required' => 'El campo :attribute es obligatorio',
			'between' => 'El :attribute valor :input no esta entre :min - :max.',
			'max' => ':attribute puede tener :max caracteres',
			'min' => ':attribute debe tener :min caracteres',
			
		]);
    	if ($validacion->fails()){
				return redirect('nuevaIncidencia')
				->withErrors($validacion)
				->withInput();
				
			}else{

				$datos = new incidencia;
				$datos -> profesorID = $request->input('profesorID');
				//$datos -> email = Auth::user()->email;
				$datos -> aula = $request->input('aula');
				$datos -> fecha = $request->input('fecha');
				$datos -> equipo = $request->input('equipo');
				$datos -> codigo = $request->input('codigo');
				$datos -> descripcion = $request->input('descripcion');
				$datos -> estado = $request->input('estado');

				$datos -> save();


        $admin=profesores::where('admin','1')->first();
 
		//Envio de correo al admin sobre la creacion de la incidencia
        Mail::send('vistaMailAdmin', ['request' => $request], function (Message $mensaje) use ($admin){
        $mensaje->to( $admin->email, 'Administrador')->subject('Reporte de incidencia');
        });
		
		//Si es admin redirige a una vista, sino a otra
		/*
        if($admin->admin == '1'){
          return redirect('inicioAdmin')->with('message', 'Incidencia registrada! ; )');
        }else{
          return redirect('inicio')->with('message', 'Incidencia registrada! ; )');
        } */

		return redirect('inicio')->with('message', 'Incidencia registrada! ; )');


		}
	}
}
