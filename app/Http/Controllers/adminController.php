<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Mail;
use App\login;
use Illuminate\Mail\Message;
use App\Incidencia as incidencia;

class adminController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    //Seleccion de la incidencia segun su id en el panel de admin
    public function editar(Request $request){
      $id=(int)$request->input('id');
      $incidencia = DB::select('Select * from incidencias where id = "'.$id.'"');
      return view('editAdminIncidencia', ['incidencia' => $incidencia]);
    }

    //Funcion para la modificacion de la incidencia
    public function actualizarDatosAdmin (Request $request){

      //si el campo no esta vacio, se procede a realizar el update
      if ($request->aula!="")
            $update=DB::table('incidencias')->where('id', '=', $request->id)
      ->update(['aula' => $request->aula]);

      if ($request->equipo!="")
           $update=DB::table('incidencias')->where('id', '=',$request->id)
     ->update(['equipo' => $request->equipo]);

     if ($request->estado!="")
          $update=DB::table('incidencias')->where('id', '=', $request->id)
     ->update(['estado' => $request->estado]);

     if ($request->codigo!="")
            $update=DB::table('incidencias')->where('id', '=', $request->id)
      ->update(['codigo' => $request->codigo]);

      if ($request->descripcion!="")
           $update=DB::table('incidencias')->where('id', '=', $request->id)
      ->update(['descripcion' => $request->descripcion]);

      if ($request->mas_info!="")
           $update=DB::table('incidencias')->where('id', '=', $request->id)
      ->update(['mas_info' => $request->mas_info]);

     if ($request->comentarios_admin!="")
          $update=DB::table('incidencias')->where('id', '=', $request->id)
      ->update(['comentarios_admin' => $request->comentarios_admin]);

      $incidencia = DB::table('incidencias')-> where('incidencias.id','=', $request->id)->first();
      $usuario = DB::table('profesores')-> where('id','=', $incidencia->profesorID)->first();
      

      //Manda correo sobre la modificacion
      if('ik012108bhe@plaiaundi.net' == auth()->user()->email){
        Mail::send('vistaMail', ['request' => $request], function (Message $mensaje) use ($usuario){
        $mensaje->to( $usuario->email, $usuario->name )->subject('Reporte de incidencia');
        });

      }else{
        Mail::send('vistaMailAdmin', ['request' => $request], function (Message $mensaje){
        $mensaje->to( 'ik012108bhe@plaiaundi.net', 'Administrador')->subject('Reporte de incidencia');
        });
      }
      return redirect('inicioAdmin')->with('alert','Incidencia modificada correctamente' );

    }




}
