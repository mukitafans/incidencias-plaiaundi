<?php

namespace App\Http\Controllers;
use DB;
use socialite;
use Mail;
use Illuminate\Mail\Message;
use App\incidencia as incidencia;
use Illuminate\Http\Request;
use Illuminate\support\facades\auth;
use Validator;



//Controlador de Modificar
class ModController extends Controller
{

      //Con la siguiente funcion, borra la incidencia en funcion de la ID
    public function EliminarIncidencia(Request $request){
      $post = incidencia::find($request->id);
        $this->authorize('delete', $post);


      $inciborrar = incidencia::select('id','fecha','aula','codigo','equipo','descripcion','estado','mas_info', 'comentarios_admin', 'profesorID',)-> where('id', $request->id)->first();
      
      if($inciborrar->estado!="Resuelta") {
          return view("VistaError",['error'=>"No puedes borrar una incidencia que aun no ha sido resuelta"]);
      }
      //if(Auth::user()->id==$inciborrar->profesorID) {
            $inciborrar->delete();
      //}else{
      //    return view("VistaError",['error'=>"La incidencia no es tuya"]);
      //}
      
      return redirect('verIncidencia')->with('alert','Incidencia modificada correctamente' );

    }

    //se seleccionan los datos de las incidencias
    public function detallesInci(Request $request){
      
      $post = incidencia::find($request->id);
        $this->authorize('view', $post);
    
      $incidencia = incidencia::select('id','fecha','aula','codigo','equipo','descripcion','estado','mas_info', 'comentarios_admin',)-> where('id',$request->id)->get();

      return view('detallesIncidencia', ['incidencia' => $incidencia]);
     
    }

   
    public function actualizarDatos (Request $request){
      $post = incidencia::find($request->id);
      $this->authorize('update', $post);


       //Seleccion de los datos de la incidencia
      $incidencia = incidencia::select('id','fecha','aula','codigo','equipo','descripcion','estado','mas_info', 'comentarios_admin',)-> where('id',$request->id)->first();
          

      $validacion = Validator::make($request->all(), [
            'equipo' => [
                  'required','min:3','max:10','regex:/^HZ[0-9]*$/',
            ],
            'aula' =>[
                  'required','min:3','max:5','regex:/^[0-9]*$/',
            ],
            'codigo' =>[
                  'required'
            ],
            'descripcion' =>[
                  'max:255',
            ],
            'profesorID' =>[
                  'required',
            ],
            'fecha' => [
                  //'required','min:10','max:10','regex:/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/',
            ],
      ],
      [
            'equipo.regex' => 'El equipo debe tener HZ mayuscula y un maximo de 9 numeros',
            'fecha.regex' => 'El formato de fecha no es valido',
            'aula.regex' => 'Solo puedes poner aulas existentes',
            'codigo.in' =>'El codigo no es valido',
            'descripcion.max' => 'La descripcion admite :max caracteres como maximo',
            'required' => 'El campo :attribute es obligatorio',
            'between' => 'El :attribute valor :input no esta entre :min - :max.',
            'max' => ':attribute puede tener :max caracteres',
            'min' => ':attribute debe tener :min caracteres',
            
      ]);
            if ($validacion->fails()){
                  return view('detallesIncidencia')
                  ->withErrors($validacion)
                  ->withInput();
                  
            }else{

           
       //Mientras no esté vacio el campo, se procede a modificar los datos
             if ($request->aula!="")
                  $update=DB::table('incidencias')->where('id', $request->id)
            ->update(['aula' => $request->aula]);

            if ($request->equipo!="")
                 $update=DB::table('incidencias')->where('id', $request->id)
           ->update(['equipo' => $request->equipo]);
            
           if ($request->codigo!="")
           $update=DB::table('incidencias')->where('id', $request->id)
            ->update(['codigo' => $request->codigo]);
            if ($request->estado!="")
           $update=DB::table('incidencias')->where('id', $request->id)
            ->update(['estado' => $request->estado]);

            if ($request->mas_info!="")
                  $update=DB::table('incidencias')->where('id', $request->id)
            ->update(['mas_info' => $request->mas_info]);

          
            //Una vez terminada la modificacion, se procede a mandar un correo con los datos
				if('ik012108bhe@plaiaundi.net' == auth()->user()->email){
					Mail::send('vistaMail', ['request' => $request], function (Message $mensaje){
					$mensaje->to( auth()->user()->email, auth()->user()->name )
							->from('info@plaiaundi.eus','Gestor de Incidencias - Plaiaundi')
							->subject('Reporte de incidencia')
							;
					});

				}else{
					Mail::send('vistaMailAdmin', ['request' => $request], function (Message $mensaje){
					$mensaje->to( 'ik012108bhe@plaiaundi.net', 'Administrador' )
							->from('info@plaiaundi.eus','Gestor de Incidencias - Plaiaundi')
							->subject('Reporte de incidencia')
							;
					});
				}
                  }
                        return redirect('verIncidencia')->with('alert','Incidencia modificada correctamente' );
          }
     
}
