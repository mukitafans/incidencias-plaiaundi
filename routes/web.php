<?php

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Verificacion de usuario Google de plaiaundi
Route::get('auth/google', 'GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'GoogleController@handleGoogleCallback');
Route::get('auth/logout', 'Auth\AuthController@logout');

//Usuario

//Ruta de inicio
Route::get('inicio','inicioController@inicio')->name('inicio');

//Creacion de la incidencia
Route::get('nuevaIncidencia','incidenciasController@formNuevaInci')->name('nuevaIncidencia');

//Guardado de la incidencia
Route::post('guardarDatos','incidenciasController@guardarDatos')->name('guardarDatos');

//Ver Lista de incidencias
Route::get('verIncidencia','incidenciasController@verInci')->name('verIncidencia');

//Modificar Incidencia
Route::post('detallesIncidencia','ModController@detallesInci')->name('detallesIncidencia');

//Guardar datos de la modificacion
Route::get('actualizarDatos','ModController@actualizarDatos')->name('actualizarDatos');

//Eliminar incidencia
Route::post('eliminarIncidencia','ModController@EliminarIncidencia')->name('eliminarIncidencia');


//Administrador
//Inicio del administrador con middleware
Route::get('inicioAdmin','inicioController@inicioAdmin')->middleware('admin')->name('inicioAdmin');

//Edicion de las incidencias del admin
Route::get('editAdminIncidencia','adminController@editar')->middleware('admin')->name('editAdminIncidencia');

//Actualizar datos de incidencia
Route::post('actualizarDatosAdmin','adminController@actualizarDatosAdmin')->middleware('admin')->name('actualizarDatosAdmin');